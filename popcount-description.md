<!-- Approved licence: YES -->
<!-- No Proprietary Software: YES -->
<!-- How long has your team been accepting publicly submitted contributions? 
  More than 2 years
-->
<!-- How many regular contributors does your team have?
  11-20 people 
-->

## Project short title

Implementing a 'popcount' primitive in the OCaml compiler.

## Long description

<code>popcount</code> counts the number of bits set to 1 in a machine
word. It is not common in everyday programming, but is useful in
some specific applications:

<ul>
<li>certain advanced cryptographic or machine learning routines</li>
<li>chess engines</li>
<li>in the implementation of some data structures such as
  Hash-Array-Mapped-Tries (HAMT; a representation of maps/dictionaries
  that is heavily used in the Clojure standard library)</li>
</ul>

It is a low-level operation that can be implemented in software, but
is also supported as a primitive by certain processor instruction
sets. For programs that heavily rely on <code>popcount</code>, using
hardware instructions can give large performance boosts.

The aim of this project is to implement a <code>popcount</code>
primitive in the OCaml compiler, that could be translated to
a hardware instruction on the architectures that support it, and
otherwise emulated in software. One would then try to use it in OCaml
programs that need popcount, and benchmark the performance
advantages.


# Minimum system requirements

It is easier to experiment with <code>popcount</code> instructions if
your machine has them. The first Intel and AMD machines with
<code>popcount</code> support were released in 2007, so more recent
machines should support it.

On a Linux system, you can check whether your machine has
a <code>popcount</code> instruction (the assembly primitive is called
<code>popcnt</code>) by checking that the following command returns
a non-empty output (if it returns nothing, you don't have support,
otherwise you do):

<code><pre>cat /proc/cpuinfo  | grep popcnt</pre></code>

The OCaml compiler implementation itself builds in a few minutes on
most laptops, and can be built under most operating systems (Windows,
OSX, Linux, BSD, etc.).

# How can applicants make a contribution to your project?

<!-- included from newcomer-jobs-2020.html -->

# Repository

<https://github.com/ocaml/ocaml>

# Issue tracker

<https://github.com/ocaml/ocaml/issues>

# Newcomer issue tag

newcomer-job

# Intern tasks

The intern would be expected to implement a <code>popcount</code>
primitive in the OCaml compiler. One could start by always using an
"emulated" software implementation, test this, and then trying to emit
the hardware instruction on hardwares machines that we can test.

Once we have a working experimental version of the compiler with
<code>popcount</code> support, we should try to measure the
performance impact on OCaml programs using
<code>popcount</code>. First we would have to look for such programs
(or possibly write simple ones ourselves if the existing codebase
are lacking), and then try to measure the performance impact of
hardware support for <code>popcount</code>.

If the performance numbers are interesting, and the invasiveness of
the implementation in the compiler codebase is not too large, we could
then propose the new primitive for inclusion in the upstream compiler.

# Intern benefits

The intern would learn to work on the codebase of a production
compiler, on an aspect that touches all layers from the frontend to
the backend (without being too complex in each layer). They would also
get some experience of performance measurements for
a performance-motivated change.

# Community benefits

The OCaml community would benefit from support for popcount
instructions in the OCaml compiler. Maybe more importantly, we would
gain a new person experienced in contributed to the compiler.

<!-- Unapproved license description:

None (the Outreachy people reviewed our CLA process in the past
-->

<!-- Unapproved license description:

None (the Outreachy people reviewed our CLA process in the past
-->

<!-- Proprietary software description:
None
-->

<!-- Accepting New Contributors?
Yes: My project is open to new contributors -->
